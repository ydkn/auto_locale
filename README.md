# auto_locale

Automatically set the locale from the browsers HTTP_ACCEPT_LANGUAGE header.

## Installation

Add this line to your application's Gemfile:

    gem 'auto_locale'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install auto_locale

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
