# frozen_string_literal: true

module AutoLocale
  # Extension to ActionController
  module ActionController
    def self.included(base)
      base.before_action :set_auto_locale
    end

    def set_auto_locale
      available_locales = I18n.available_locales.map(&:to_s)

      auto_locale_request_locales.each do |l|
        available_locales.each do |al|
          if l == al
            I18n.locale = al.to_sym

            return nil
          elsif l.split('-', 2).first == al.split('-', 2).first
            I18n.locale = al.to_sym

            return nil
          end
        end
      end
    rescue StandardError
      I18n.locale = I18n.default_locale
    end

    private

    def auto_locale_request_locales # rubocop:disable Metrics/AbcSize
      locales = request.accept_language.split(/\s*,\s*/).map do |l|
        l += ';q=1.0' unless l.match?(/;q=\d+\.\d+$/)
        l.split(';q=')
      end

      locales.sort! { |a, b| b.last.to_f <=> a.last.to_f }

      locales.map { |l| l.first.downcase.gsub(/-[a-z]+$/i, &:upcase) }
    end
  end
end
