# frozen_string_literal: true

require 'action_controller/base'
require 'auto_locale/action_controller'

ActionController::Base.send(:include, AutoLocale::ActionController)
