# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'auto_locale/version'

Gem::Specification.new do |spec|
  spec.name        = 'auto_locale'
  spec.version     = AutoLocale::VERSION
  spec.authors     = ['Florian Schwab']
  spec.email       = ['me@ydkn.de']
  spec.homepage    = 'https://gitlab.com/ydkn/auto_locale'
  spec.summary     = 'Automatically set the locale from the browsers HTTP_ACCEPT_LANGUAGE header'
  spec.description = 'Automatically set the current locale from the browsers HTTP_ACCEPT_LANGUAGE header'

  spec.files         = `git ls-files`.split("\n")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'actionpack'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
end
